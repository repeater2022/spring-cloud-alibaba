package com.zjy.dataprovider8000.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name = "测试用例")
@RestController
@RefreshScope
public class TestController {


    @Value(value="${environment:}")
    private String value;
    @Operation(summary = "测试配置读取")
    @GetMapping("/test")
    public String test(){



        return value;
    }


}
