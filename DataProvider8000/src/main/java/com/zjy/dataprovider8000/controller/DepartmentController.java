package com.zjy.dataprovider8000.controller;



import com.alibaba.csp.sentinel.annotation.SentinelResource;

import com.zjy.dataprovider8000.service.DepartmentService;
import com.zjy.pojo.Department;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;



@RestController
@Tag(name="部门")
@RequestMapping("/dept")
public class DepartmentController {
    @Resource
    DepartmentService departmentService;

    @Operation(summary = "获取部门列表")
    @GetMapping("/list")
    @SentinelResource(value="获取部门列表")
    @Cacheable(value="redisCache")
    public List<Department> getAll(){
        return departmentService.getDepartmentList();
    }


    @Operation(summary = "通过id查找部门")
    @GetMapping("/query/id/{departmentId}")
    @SentinelResource
    public Department queryById(@PathVariable Integer departmentId){
        Department dept=departmentService.getDepartmentById(departmentId);
        if(dept.getDepartmentName()==null)
            throw new NullPointerException("there's no such a dept!"){
        };
        return dept;
    }

    @Operation(summary = "通过名称查找部门")
    @GetMapping("/query/name/{departmentName}")
    @SentinelResource(value = "getDepartmentById")
    public List<Department> queryByName(@PathVariable String departmentName){
        return departmentService.getDepartmentByName(departmentName);
    }
}
