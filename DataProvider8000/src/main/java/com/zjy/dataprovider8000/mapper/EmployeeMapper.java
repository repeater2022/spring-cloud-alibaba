package com.zjy.dataprovider8000.mapper;

import com.zjy.pojo.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
@Repository
public interface EmployeeMapper {

    List<Employee> getEmployeeList();

    Employee getEmployeeById(Integer employeeId);

    boolean insert(Employee employee);

    boolean insert(List<Map> employee);

    Employee deleteById(Integer employeeId);

    Employee update(Employee employee);

    Employee update(List<Map> employee);

}
