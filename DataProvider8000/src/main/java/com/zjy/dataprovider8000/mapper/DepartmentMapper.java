package com.zjy.dataprovider8000.mapper;


import com.zjy.pojo.Department;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface DepartmentMapper {
    List<Department> getDepartmentList();
    Department getDepartmentById(Integer departmentId);

    List<Department> getDepartmentByName(String departmentName);
    void insert(Department department);
    void deleteDepartmentById(Integer departmentId);
    void update(Department department);
}
