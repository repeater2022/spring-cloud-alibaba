package com.zjy.dataprovider8000.service;



import com.zjy.dataprovider8000.mapper.DepartmentMapper;
import com.zjy.pojo.Department;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DepartmentService {
    @Resource
    private DepartmentMapper departmentMapper;

    public List<Department> getDepartmentList(){
        System.err.println("Service：getDepartmentList...");
        return departmentMapper.getDepartmentList();
    }
    public Department getDepartmentById(Integer departmentId){
        return departmentMapper.getDepartmentById(departmentId);
    }
    public List<Department> getDepartmentByName(String departmentName){
        return departmentMapper.getDepartmentByName(departmentName);
    }
    public void insert(Department department){
         departmentMapper.insert(department);
    }
    public void deleteDepartmentById(Integer departmentId){
        departmentMapper.deleteDepartmentById(departmentId);
    }
    public void update(Department department){
        departmentMapper.update(department);
    }
}
