package com.zjy.dataprovider8000;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@EnableCaching
//@MapperScan("com.zjy.dataprovider8000.mapper")
//上面这条注释不加有时会报错，加了之后再注释掉莫名其妙的也不报错了，存疑
public class DataProvider8000Application {

    public static void main(String[] args) {
        SpringApplication.run(DataProvider8000Application.class, args);
    }

}
