package com.zjy.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Department implements Serializable {
    private Integer departmentId ;
    private String departmentName;
    private Integer managerId;
    private Integer locationId;


}
