package com.zjy.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {

    private Integer employeeId;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String jobId;
    private double salary;
    private double commissionPct;
    private Integer managerId;
    private Integer departmentId;
    private Date hireDate;


    public String getName(){
        return firstName+" "+lastName;
    }

}
