package com.zjy.security.mapper;


import com.zjy.security.pojo.MyUser;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserMapper {

    List<MyUser> getAll();

    MyUser getUserByName(String username);
}
