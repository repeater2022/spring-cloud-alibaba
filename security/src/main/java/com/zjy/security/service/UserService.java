package com.zjy.security.service;


import com.zjy.security.mapper.UserMapper;
import com.zjy.security.pojo.MyUser;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserService {
    @Resource
    private UserMapper userMapper;

    public List<MyUser> getAll(){
       return userMapper.getAll();
    }

    public MyUser getUserByName(String username){
       return userMapper.getUserByName(username);
    }
}
