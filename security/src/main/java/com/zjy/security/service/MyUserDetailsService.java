package com.zjy.security.service;


import com.zjy.security.mapper.UserMapper;
import com.zjy.security.pojo.MyUser;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {
    @Resource
    private UserMapper usersMapper;

    AuthenticationManager authenticationManager(){
        return new OAuth2AuthenticationManager();
    }
    @Override
    public UserDetails loadUserByUsername(String username) throws
            UsernameNotFoundException {

        MyUser user=usersMapper.getUserByName(username);
        if(user == null) {
            throw new UsernameNotFoundException("用户名不存在！");
        }
        System.out.println(user);

        //角色需要前缀“ROLE_”，权限不需要，角色和权限全部写进一个字符串，用逗号分隔
        List<GrantedAuthority> auths =
                AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_vip1,ROLE_vip2");
        //**********************************
        //如果权限和角色也写进数据库呢？
        //查出角色和权限后，遍历进行如下写入：
        //auths.add(new SimpleGrantedAuthority(" "));
        //***********************************
        return new User(user.getUsername(),
                new BCryptPasswordEncoder().encode(user.getPassword()),
                auths);
    }
}