package com.zjy.security.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyUser  {
    private String username;
    private String password;
    private int id;
    private String salt;
}
