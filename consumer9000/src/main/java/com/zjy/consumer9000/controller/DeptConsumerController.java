package com.zjy.consumer9000.controller;


import com.zjy.consumer9000.service.DeptFeignService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@Tag(name="consumer for dept")
@RequestMapping("/dept")
public class DeptConsumerController {
    @Resource
    private DeptFeignService deptFeignService;


    @Operation(summary="本地测试")
    @GetMapping("/test")
    public String test(){
        return "OK";
    }
    @Operation(summary = "获取部门列表")
    @GetMapping("/list")
    public List<Map> getAll(){
        System.err.println("listing all departments...");
        return deptFeignService.getAll();
    }

    @Operation(summary = "通过id查找部门")
    @GetMapping("/query/id/{departmentId}")
    public Map queryById(@PathVariable Integer departmentId){return deptFeignService.queryById(departmentId);}
    @Operation(summary = "通过名称查找部门")
    @GetMapping("/query/name/{departmentName}")
    public List<Map> queryByName(@PathVariable String departmentName){
        return deptFeignService.queryByName(departmentName);
    }

}
