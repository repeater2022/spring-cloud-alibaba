package com.zjy.consumer9000.service;

import com.zjy.consumer9000.config.DeptFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;

@FeignClient(value = "dataProvider8000",fallbackFactory = DeptFallbackFactory.class)
@Component
public interface DeptFeignService {

    @GetMapping("/dept/list")
    List<Map> getAll();

    @GetMapping("/dept/query/id/{departmentId}")
    Map queryById(@PathVariable("departmentId") Integer departmentId);

    @GetMapping("/dept/query/name/{departmentName}")
    List<Map> queryByName(@PathVariable("departmentName") String departmentName);

}
