package com.zjy.consumer9000.config;

import com.zjy.consumer9000.service.DeptFeignService;
import feign.hystrix.FallbackFactory;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeptFallbackFactory implements FallbackFactory<DeptFeignService> {
    @Override
    public DeptFeignService create(Throwable cause) {
        return new DeptFeignService() {
            @Override
            public List<Map> getAll() {
                Map map=new HashMap();
                map.put("msg","unknown exception");
                List<Map> list=new ArrayList<>();
                list.add(map);
                return list;
            }

            @Override
            public Map queryById(Integer departmentId) {
                Map map=new HashMap();
                map.put("msg","not found");
                return map;
            }

            @Override
            public List<Map> queryByName(String departmentName) {
                Map map=new HashMap();
                map.put("msg","there's no such a department");
                List<Map> list=new ArrayList<>();
                list.add(map);
                return list;
            }
        };
    }
}
