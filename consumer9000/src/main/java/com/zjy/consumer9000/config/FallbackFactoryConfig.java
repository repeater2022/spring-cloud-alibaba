package com.zjy.consumer9000.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FallbackFactoryConfig {
    @Bean
    public DeptFallbackFactory getDeptFallbackFactory(){
        return new DeptFallbackFactory();
    }
}
